#
# rpi_train.py
# Francesco Conti <fconti@iis.ee.ethz.ch>
# Alfio Di Mauro <adimauro@iis.ee.ethz.ch>
#
# Copyright (C) 2018-2019 ETH Zurich
# All rights reserved
#

import torch
import numpy as np
from tqdm import tqdm
from copy import deepcopy

import h5py
from te_metrics import *
from te_BatchIterator import *

def rpi_func_build(train_h5=None, val_h5=None, regime=None, tbx_writer=None, tbx_beholder=None, train_loader=None, use_cuda=True, optimizer=None, criterion=None, lq_quant=False, transpose=False, marginalize=False, task='classification', verbose=True, shuffle=True, normalize_range=True):

    if use_cuda:
        if transpose:
            perm = lambda x: x.permute(0,3,1,2).float().cuda()
        else:
            perm = lambda x: x.float().cuda()
        if marginalize:
            marg = lambda y: y.max(dim=1)[1].cuda()
        else:
            marg = lambda y: y.cuda()
    else:
        if transpose:
            perm = lambda x: x.permute(0,3,1,2).float()
        else:
            perm = lambda x: x.float()
        if marginalize:
            marg = lambda y: y.max(dim=1)[1]
        else:
            marg = lambda y: y

    indeces = np.arange(train_h5['y_data'].shape[0])
    if shuffle:
        np.random.shuffle(indeces)

    def rpi_train(model, epoch, criterion=criterion, freeze_bn=False, normalize_range=normalize_range):

        train_loader = BatchIterator(train_h5, indeces, train_h5['X_data'].shape[1:], train_h5['y_data'].shape[-1], regime['batch_size'], 'classification') # FIXME task type

        model.train()
        if freeze_bn:
            if use_cuda:
                try:
                    model_inner = model.module
                except AttributeError:
                    model_inner = model
            else:
                model_inner = model
            model_inner.freeze_bn()
        train_loss = Metric('train_loss')
        train_accuracy = Metric('train_accuracy')

        with tqdm(total=train_loader.get_max_lim(),
                desc='Train Epoch     #{}'.format(epoch + 1)) as t:
            for batch_idx, x in enumerate(train_loader):
                ff = list(x)
                # shamelessly adapted from training engine TrainRunner.py
                inputs  = torch.from_numpy(np.array([np.reshape(f[0], [train_h5['X_data'].shape[1], train_h5['X_data'].shape[2], train_h5['X_data'].shape[3]]) for f in ff]))
                targets = torch.from_numpy(np.array([f[1] for f in ff])) # FIXME -- task type
                # outputs = self.network(torch.from_numpy(np.array([np.reshape(f[0], [self.img_size[0], self.img_size[1], self.img_size[2]]) for f
                #                         in ff]).transpose((0,3,1,2))).float().to(self.device))
                # if self.task_type == 'detection':
                #     loss = self.network.return_loss(outputs, [f[1] for f in ff], self.num_classes).to(self.device)
                #     valid_step_accuracy = 0
                # else:
                #     loss = self.network.return_loss(outputs, torch.from_numpy(np.array([f[1] for f in ff])).long().to(self.device)).to(self.device)
                #     valid_step_accuracy = self.network.return_accuracy(outputs, torch.from_numpy(
                #         np.array([f[1] for f in ff])).to(self.device),  self.num_classes,self.task_type).to(self.device)

                inputs, targets = perm(inputs), marg(targets)

                # FIXME: this is suboptimal!!! change to something better...
                if normalize_range:
                    inputs /= inputs.max()

                optimizer.zero_grad()
                output = model(inputs)
                loss = criterion(output, targets)
                loss.backward()
                optimizer.step()
                train_loss.update(loss)
                train_accuracy.update(accuracy(output, targets))
                t.set_postfix({'loss': train_loss.avg.item(),
                            'accuracy': 100. * train_accuracy.avg.item()})
                t.update(1)

        if tbx_writer:
            tbx_writer.add_scalar('train/loss', train_loss.avg, epoch)
            tbx_writer.add_scalar('train/accuracy', train_accuracy.avg, epoch)

        return train_loss.avg

    def rpi_validate(model, epoch, val_loader=None, criterion=criterion, freeze_bn=False, normalize_range=normalize_range, eps_in=None):

        val_loader   = BatchIterator(val_h5,   np.arange(val_h5['y_data'].shape[0]),   val_h5['X_data'].shape[1:],   val_h5['y_data'].shape[-1],   regime['batch_size'], 'classification') # FIXME task type

        model.eval()
        if freeze_bn:
            if use_cuda:
                try:
                    model_inner = model.module
                except AttributeError:
                    model_inner = model
            else:
                model_inner = model
            model_inner.freeze_bn()
        val_loss = Metric('val_loss')
        val_accuracy = Metric('val_accuracy')

        with tqdm(total=val_loader.get_max_lim(),
                desc='Validate Epoch  #{}'.format(epoch + 1)) as t:
            with torch.no_grad():
                for batch_idx, x in enumerate(val_loader):
                    ff = list(x)
                    # shamelessly adapted from training engine TrainRunner.py
                    inputs  = torch.from_numpy(np.array([np.reshape(f[0], [val_h5['X_data'].shape[1], val_h5['X_data'].shape[2], val_h5['X_data'].shape[3]]) for f in ff]))
                    targets = torch.from_numpy(np.array([f[1] for f in ff])) # FIXME -- task type
                    # outputs = self.network(torch.from_numpy(np.array([np.reshape(f[0], [self.img_size[0], self.img_size[1], self.img_size[2]]) for f
                    #                         in ff]).transpose((0,3,1,2))).float().to(self.device))
                    # if self.task_type == 'detection':
                    #     loss = self.network.return_loss(outputs, [f[1] for f in ff], self.num_classes).to(self.device)
                    #     valid_step_accuracy = 0
                    # else:
                    #     loss = self.network.return_loss(outputs, torch.from_numpy(np.array([f[1] for f in ff])).long().to(self.device)).to(self.device)
                    #     valid_step_accuracy = self.network.return_accuracy(outputs, torch.from_numpy(
                    #         np.array([f[1] for f in ff])).to(self.device),  self.num_classes,self.task_type).to(self.device)

                    inputs, targets = perm(inputs), marg(targets)
                    
                    # FIXME: this is suboptimal!!! change to something better...
                    if normalize_range:
                        inputs /= inputs.max()

                    if eps_in is not None:
                        inputs = (inputs/eps_in).round()
                    
                    output = model(inputs)

                    val_loss.update(criterion(output, targets))
                    val_accuracy.update(accuracy(output, targets))
                    del output, inputs, targets
                    t.set_postfix({'loss': val_loss.avg.item(),
                                'accuracy': 100. * val_accuracy.avg.item()})
                    t.update(1)

        if tbx_writer:
            if use_cuda:
                try:
                    model_inner = model.module
                except AttributeError:
                    model_inner = model
            else:
                model_inner = model
            tbx_writer.add_scalar('val/loss', val_loss.avg, epoch)
            tbx_writer.add_scalar('val/accuracy', val_accuracy.avg, epoch)
            for name, param in model_inner.named_parameters():
                if 'bn' not in name:
                    try:
                        tbx_writer.add_histogram(name, param, epoch)
                    except TypeError:
                        pass
        
        return val_accuracy.avg

    return rpi_train, rpi_validate

class Metric(object):
    def __init__(self, name):
        self.name = name
        self.sum = torch.tensor(0.)
        self.n = torch.tensor(0.)

    def update(self, val):
        self.sum += val.detach().cpu()
        self.n += 1

    @property
    def avg(self):
        return self.sum / self.n

def accuracy(output, target):
    # get the index of the max log-probability
    pred = output.max(1, keepdim=True)[1]
    return pred.eq(target.view_as(pred)).cpu().float().mean()
