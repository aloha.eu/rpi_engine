#
# rpi_engine.py
# Francesco Conti <fconti@iis.ee.ethz.ch>
# Alfio Di Mauro <adimauro@iis.ee.ethz.ch>
#
# Copyright (C) 2018-2020 ETH Zurich
# All rights reserved
#

from __future__ import print_function

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torch.backends.cudnn as cudnn

import torchvision
import torchvision.transforms as transforms

import os, datetime, sys
import argparse

from torch.autograd import Variable
from rpi_train import *
from onnx2pytorch.onnx2pytorch import onnxextract
#from onnx2pytorch.npz_to_pth import npz_to_state_dict
from tqdm import tqdm

import h5py
from te_metrics import *
from te_BatchIterator import *

import nemo

import logging
import json
import numpy as np

# default folders
SHARED_DATA_FOLDER = "./"
LOGGING_FOLDER = "./"
DEFAULT_ARGS = {
    'onnx_path':             None,
    'regime':                None,
    'log_dir':               LOGGING_FOLDER,
    'eval':                  False,
    'export':                False,
    'export_without_deploy': False,
    'checkpoint_freq':       50,
    'dataset':               None,
    'loss':                  'softmax',
    'evaluate_exhaustive':   True,
    'min_prec_dict':         None,
    'dry_run':               False,
    'tensor_shape':          "NCHW",
    'output_folder':         ".",
    'input_shape':           None,
    'input_range':           (0, 1),
    'input_bits':            8,
    'freeze_bn':             False,
    'eval_threshold':        0.95
}
ACTION_ARGS = {
    'eval':                  'store_true',
    'export':                'store_true',
    'export_without_deploy': 'store_true',
    'dry_run':               'store_true',
    'freeze_bn':             'store_true',
}
TYPE_ARGS = {
    'input_range':           list,
}

def rpi_engine(**kwargs):

    # default values
    args=DEFAULT_ARGS
    #replaces the default values with possibly passed parameters
    args={**args, **kwargs}
    if type(args['input_shape']) is str:
        from ast import literal_eval
        args['input_shape'] = literal_eval(args['input_shape'])
    
    os.makedirs(os.path.join(args['output_folder'], "RPI"), exist_ok=True)
    output_onnx = os.path.join(args['output_folder'], "RPI","quantized_model.onnx") #TODO: fix name adding qunatization values
    output_info = os.path.join(args['output_folder'], "RPI","report.log")

    # set up logging
    log_name = args['log_dir'] + '/{date:%Y_%m_%d_%H%M%S}.txt'.format( date=datetime.datetime.now())
    logging.basicConfig(level=logging.INFO,
                format="%(asctime)s - %(levelname)s - %(message)s",
                datefmt="%Y-%m-%d %H:%M:%S",
                filename=os.path.join(args['log_dir'], log_name),
                filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    # set up tbx
    from tensorboardX import SummaryWriter
    tbx_name = log_name[4:-4]
    tbx_writer = SummaryWriter(log_dir='runs/%s' % tbx_name)
    tbx_beholder = None

    # debug: log args
    logging.info("[RPI] Arguments: %s" % args)

    # load regime from JSON file
    regime = {}
    if args['regime'] is None:
        logging.info("ERROR!!! Missing regime JSON.")
        raise Exception
    else:
        with open(args['regime'], "r") as f:
            rr = json.load(f)
        for k in rr.keys():
            try:
                regime[int(k)] = rr[k]
            except ValueError:
                regime[k] = rr[k]

    use_cuda = torch.cuda.is_available()

    # Dataset loader
    logging.info('[RPI] Loading dataset')
    loader_params = {'batch_size': 128, 'shuffle': True, 'num_workers': 4} # FIXME put into regime
    train_h5 = h5py.File(args['dataset'] + "train")
    val_h5 = h5py.File(args['dataset'] + "valid")
    train_loader = BatchIterator(train_h5, np.arange(train_h5['y_data'].shape[0]), train_h5['X_data'].shape[1:], train_h5['y_data'].shape[-1], regime['batch_size'], 'classification') # FIXME task type
    val_loader   = BatchIterator(val_h5,   np.arange(val_h5['y_data'].shape[0]),   val_h5['X_data'].shape[1:],   val_h5['y_data'].shape[-1],   regime['batch_size'], 'classification') # FIXME task type

    # Model construction and quantization
    logging.info('[RPI] Building PyTorch model from ONNX')
    onnxextract(args['onnx_path'], path="onnxgennet")
    import onnxgennet.Net as gennet
    net = gennet.Net()
     
    logging.info('[RPI] Replacing primitives with quantization-aware ones')
    dummy_input = torch.randn(1, *args['input_shape'], device='cpu')
    net = nemo.transform.quantize_pact(net, dummy_input=dummy_input)

    logging.info("[RPI] args:")
    logging.info(args)

    logging.info("[RPI] regime:")
    logging.info(regime)

    # CUDA-ify
    if use_cuda:
        logging.info('[RPI] Using CUDA')
        net = torch.nn.DataParallel(net).cuda()
        net_inner = net.module
    else:
        logging.info('[RPI] Not using CUDA')
        net_inner = net

    logging.info("[RPI] net:")
    logging.info(net)

    criterion_marginalize = False
    if args['loss'] == 'margin':
        criterion_marginalize = True
        criterion = nn.SoftMarginLoss()
    elif args['loss'] == 'sigmoid':
        criterion_marginalize = True
        criterion = nn.BCEWithLogitsLoss()
    elif args['loss'] == 'softmax':
        criterion_marginalize = True
        criterion = nn.CrossEntropyLoss()
    elif args['loss'] == 'dice_loss':
        criterion = te_metrics.DiceLoss()
    elif args['loss'] == 'detection_loss':
        criterion = te_metrics.YoloLoss()
    else:
        raise Exception
    weight_decay = float(regime['weight_decay'])

    accuracy_values     = [] #float
    loss_values         = [] #float
    quantization_values = [] #int

    # Choose optimizer
    try:
        clip_weight_decay = float(regime['clip_weight_decay'])
    except KeyError:
        clip_weight_decay = float(regime['weight_decay'])
    if regime['optimizer'] == 'adam':
        logging.info("[RPI] Choosing Adam as optimizer with learning rate %.3e" % float(regime['lr']))
        try:
            optimizer = torch.optim.Adam( [ { 'params' : net_inner.get_nonclip_parameters() },
                                            { 'params' : net_inner.get_clip_parameters(), 'weight_decay' : clip_weight_decay } ],
                                        lr=float(regime['lr']), weight_decay=float(regime['weight_decay']))
        except AttributeError:
            optimizer = torch.optim.Adam(net_inner.parameters(), lr=float(regime['lr']), weight_decay=float(regime['weight_decay']))
    elif regime['optimizer'] == 'sgd':
        logging.info("[RPI] Choosing SGD as optimizer with learning rate %.3e" % float(regime['lr']))
        optimizer = torch.optim.SGD(net.parameters(), weight_decay=float(regime['weight_decay']), lr=float(regime['lr']))
    else:
        logging.info("unsupported optimizer")
        raise Exception

    try:
        precision_rule = nemo.utils.parse_precision_rule(regime['relaxation'])
    except KeyError:
        precision_rule = None

    # create rpi_train, rpi_validate functions
    rpi_train, rpi_validate = rpi_func_build(train_h5        = train_h5,
                                             val_h5          = val_h5,
                                             tbx_writer      = tbx_writer,
                                             tbx_beholder    = tbx_beholder,
                                             use_cuda        = torch.cuda.is_available(),
                                             optimizer       = optimizer,
                                             criterion       = criterion,
                                             lq_quant        = False,
                                             transpose       = True if args['tensor_shape']=="NHWC" else False,
                                             marginalize     = criterion_marginalize,
                                             regime          = regime,
                                             verbose         = False,
                                             shuffle         = True,
                                             normalize_range = True)
                                             # FIXME : extend rpi_func_build to support post-integerization validation
                                             # integer      = False,
                                             # input_range  = args['input_range'],
                                             # input_bits   = args['input_bits'])

    logging.info("[RPI] Gather statistics for activations")
    net_inner.change_precision(bits=20)
    net_inner.set_statistics_act()
    acc = rpi_validate(net, 0, criterion=criterion)
    net_inner.unset_statistics_act()
    net_inner.reset_alpha_act()
    logging.info("[RPI] %.2f%%" % (100*acc.item()))

    # MAIN OPERATION MODE
    loss_epoch_m1 = 1e9

    curr_base_lr = regime['lr']
    for p in optimizer.param_groups:
        p['lr'] = curr_base_lr

    # Perform exhaustive evaluation of accuracy without retraining
    if args['evaluate_exhaustive']:
        logging.info("[RPI] Performing exhaustive accuracy evaluation")
        def net_rpi_validate(epoch, **kwargs):
            return rpi_validate(net, epoch, **kwargs)
        evale = nemo.evaluation.EvaluationEngine(net_inner, precision_rule=precision_rule, validate_fn=net_rpi_validate, validate_data=val_loader) # FIXME remove val_loader dependncy
        while evale.step():
            acc = rpi_validate(net, 0, criterion=criterion)
            evale.report(acc)
            logging.info("[RPI] %.1f-bit W, %.1f-bit x: %.2f%%" % (evale.wgrid[evale.idx], evale.xgrid[evale.idx], 100*acc.item()))
            if args['eval']:
                accuracy_values.append(100*acc.item())
                loss_values.append(-1)
                quantization_values.append((evale.wgrid[evale.idx], evale.xgrid[evale.idx])) 
        Wbits, xbits = evale.get_next_config(upper_threshold=args['eval_threshold'])
        precision_rule['0']['W_bits'] = min(Wbits, precision_rule['0']['W_bits'])
        precision_rule['0']['x_bits'] = min(xbits, precision_rule['0']['x_bits'])
        logging.info("[RPI] Choosing %.1f-bit W, %.1f-bit x for first step", precision_rule['0']['W_bits'], precision_rule['0']['x_bits'])
    else:
        evale = None
    if args['min_prec_dict'] is not None:
        min_prec_dict = nemo.utils.precision_dict_from_json(args['min_prec_dict'])
    else:
        min_prec_dict = None
    relax = nemo.relaxation.RelaxationEngine(net_inner, optimizer, criterion, train_loader, precision_rule, reset_alpha_weights=False, min_prec_dict=min_prec_dict, evaluator=evale)

    if args['export'] and args['eval']:
        if not args['export_without_deploy']:
            # harden weight
            net_inner.harden_weights()
            # fold all biases inside the nearest batch-norm layers
            net_inner.remove_bias()
            # replace BN with quantized BN layers
            net = nemo.transform.bn_quantizer(net)
            # move to q-deploy representation
            input_bias = 0 if args['input_range'][0] >= 0 else -args['input_range'][0]
            eps_in = (args['input_range'][1]-args['input_range'][0])/(2**args['input_bits']-1)
            net_inner.set_deployment(eps_in)
            # move to i-deploy representation
            net = nemo.transform.integerize_pact(net, eps_in)
        logging.info("Exporting ONNX to {}".format(output_onnx))
        nemo.utils.export_onnx(output_onnx, net, net_inner, args['input_shape'])
        logging.info("DONE!")
        return {"path_to_onnx":"%s" % "None", "rpi_training_accuracy": accuracy_values, "rpi_training_loss": loss_values,   "quantization": quantization_values}

    # MAIN TRAINING LOOP
    logging.info("########### MAIN TRAINING LOOP #############")
    net.train()
    try:
        for epoch in range(0, regime['epochs']):
            
            for p in optimizer.param_groups:
                p['lr'] = regime['lr_decay'] * p['lr']
        
            for p in optimizer.param_groups:
                logging.info("[Top]\t\t lr: %.5e" % (p['lr']))
                break
        
            try:
                curr_regime = regime[epoch]
            except KeyError:
                curr_regime = None
        
            # if using multiple learning-rates, change it now
            try:
                if curr_regime is not None:
                    for p in optimizer.param_groups:
                        p['lr'] = curr_regime['lr']
            except KeyError:
                pass
        
            change_prec = False
            ended = False
            if precision_rule is not None:
                change_prec, ended = relax.step(loss_epoch_m1, epoch)

            # dry run of the main training loop
            if args['dry_run']:
                if change_prec or epoch==0:
                    loss_epoch_m1 = 5
                else:
                    loss_epoch_m1 = loss_epoch_m1 * 0.5

                accuracy_values.append(50)
                loss_values.append(loss_epoch_m1)
                quantization_values.append((net_inner.W_precision.get_bits(), net_inner.x_precision.get_bits()))
            else:
                loss_epoch_m1 = rpi_train(net, epoch, freeze_bn=False)
                acc = rpi_validate(net, epoch, criterion=criterion, freeze_bn=True)

                accuracy_values.append(100*acc.item())
                loss_values.append(loss_epoch_m1.item())
                quantization_values.append((net_inner.W_precision.get_bits(), net_inner.x_precision.get_bits()))

                logging.info("[RPI] @%d test_acc=%.2f%%" % (epoch, 100.*acc))
                nemo.utils.save_checkpoint(net_inner, optimizer, epoch, acc)

            if ended:
                logging.info("[RPI] End of training reached at epoch %d" % (epoch))
                break

    except KeyboardInterrupt:
        logging.info("[RPI] Interrupting training due to CTRL+C...")

    if args['export']:
        if not args['export_without_deploy']:
            # harden weights
            net_inner.harden_weights()
            # fold all biases inside the nearest batch-norm layers
            net_inner.remove_bias()
            # replace BN with quantized BN layers
            net = nemo.transform.bn_quantizer(net)
            # move to q-deploy representation
            input_bias = 0 if args['input_range'][0] >= 0 else -args['input_range'][0]
            eps_in = (args['input_range'][1]-args['input_range'][0])/(2**args['input_bits']-1)
            net_inner.set_deployment(eps_in)
            acc = rpi_validate(net, epoch, criterion=criterion, freeze_bn=True)
            logging.info("[RPI] @%d test_acc after deployment = %.2f%%" % (epoch, 100.*acc))
            # move to i-deploy representation
            net = nemo.transform.integerize_pact(net, eps_in)
            acc = rpi_validate(net, epoch, criterion=criterion, freeze_bn=True, eps_in=eps_in)
            logging.info("[RPI] @%d test_acc after integerization = %.2f%%" % (epoch, 100.*acc))
        nemo.utils.export_onnx(output_onnx, net, net_inner, args['input_shape'])
        with open(output_info,"w") as f:
          f.write("RPI report:\n")
          f.write("  Epochs:   {}\n".format(epoch+1))
          f.write("  Accuracy: {}\n".format(100.*acc))
          f.write("  X bits: {}  W bits: {}\n".format(net_inner.x_precision.get_bits(),net_inner.W_precision.get_bits()))
          f.write("  Evaluated points: \n")
          f.write("    Accuracy:     {}\n".format(accuracy_values))
          f.write("    Loss:         {}\n".format(loss_values))
          f.write("    quantization: {}\n".format(quantization_values))
          
        logging.info("DONE!")
    return {"path_to_onnx": "%s" % output_onnx, "rpi_training_accuracy": accuracy_values, "rpi_training_loss": loss_values,   "quantization": quantization_values}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Refinement for Parsimonious Inference -- CLI')

    # rea arguments from command line instead of JSON
    for n in DEFAULT_ARGS.keys():
        try:
            action = ACTION_ARGS[n]
        except KeyError:
            action = None
        if action is None:
            parser.add_argument('--%s' % n, default=DEFAULT_ARGS[n])
        else:
            parser.add_argument('--%s' % n, default=DEFAULT_ARGS[n], action=action)
    args = parser.parse_args()
    args_dict = vars(args)
    # condition arguments to real type
    for n in TYPE_ARGS.keys():
        args_dict[n] = TYPE_ARGS[n](args_dict[n])
    d = rpi_engine(**args_dict)
