#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 02/21/2018 16:55 $
# by : shepeleva $
# SVN : $
#

# --- imports -----------------------------------------------------------------

import cv2
import numpy as np
from sklearn.preprocessing import OneHotEncoder
#from models.models import *

class BatchIterator:

    def __init__(self, file_name, data_split, img_size, num_lbls, batch_size, task_type, augment_dict=None,
                 shuffle_key=False, colormap=None):

        # print("in batch" + project_id)
        # self.db_storage = Dataset.objects.get(project=project_id)

        self.file_name = file_name
        self.data_split = data_split
        # self.labels = OneHotEncoder().fit_transform(np.array(self.db_storage.labels)[:].reshape(-1, 1)).toarray().astype(int)

        self.task_type = task_type
        self.augment_dict = augment_dict
        self._preprocess_data(img_size, num_lbls, batch_size, colormap)

        if shuffle_key:
            self._shuffle()
        else:
            self.permutation_list = self.data_split


        if (self.task_type == 'classification') or (self.task_type == 'segmentation') or (self.task_type == 'detection'):
            self.iterator = iter(self.permutation_list[0:self.max_lim])
        else:
            raise ValueError("This functions are not yet supported")
        try:
            self.current = next(self.iterator)
        except StopIteration:
            self.on_going = False
        else:
            self.on_going = True

    def _preprocess_data(self,  img_size, num_lbls, batch_size, colormap):
        self.X_key = 'X_data'
        self.y_key = 'y_data'
        self.size = len(self.data_split)
        self.img_size = img_size
        self.num_lbls = num_lbls
        self.batch_size = batch_size

        self.max_lim = int(self.size / self.batch_size) * self.batch_size
        if self.task_type is 'segmentation':
            self.colormap = colormap

    def get_max_lim(self):
        return int(self.max_lim / self.batch_size)

    def _shuffle(self):
        import random
        self.permutation_list = random.sample(self.data_split, len(self.data_split))

    def __iter__(self):
        if self.task_type == 'classification':
            while self.on_going:
                yield self._next_batch_classification()
        elif self.task_type == 'segmentation':
            while self.on_going:
                yield self._next_batch_segmentation()
        elif self.task_type == 'prediction':
            while self.on_going:
                yield self._next_batch_prediction()
        elif self.task_type == 'detection':
            while self.on_going:
                yield self._next_batch_detection()
        elif self.task_type == 'multitask':
            while self.on_going:
                yield self._next_batch_multitask()

    def _next_batch_segmentation(self):
        # with tf.device('/cpu:0'):
        imgs = self.file_name[self.X_key][self.current]
        if self.file_name[self.y_key][:].size != 0:
            msks = self.file_name[self.y_key][self.current]
            if not list(self.img_size) == imgs[0].shape:
                imgs, msks = self._resize_data(imgs, msks)
            if self.augment_dict:
                imgs, msks = self._augment_data(imgs, msks)
            msks = self._recode_masks(msks)
            yield imgs, msks
        else:
            if not list(self.img_size) == imgs[0].shape:
                imgs = self._resize_data(imgs)
            if self.augment_dict:
                imgs = self._augment_data(imgs)
            yield imgs, None
        # start enumerate at 1 because we already yielded the last saved item
        for num, item in enumerate(self.iterator, 1):
            imgs = self.file_name[self.y_key][item]
            if self.file_name[self.y_key][:].size != 0:
                msks = self.file_name[self.y_key][item]
                if not list(self.img_size) == imgs[0].shape:
                    imgs, msks = self._resize_data(imgs, msks)
                if self.augment_dict:
                    imgs, msks = self._augment_data(imgs, msks)
                self.current = item
                if num == self.batch_size:
                    break
                msks = self._recode_masks(msks)
                yield imgs, msks
            else:
                if not list(self.img_size) == imgs[0].shape:
                    imgs = self._resize_data(imgs)
                if self.augment_dict:
                    imgs = self._augment_data(imgs)
                self.current = item
                if num == self.batch_size:
                    break
                yield imgs, None
        else:
            self.on_going = False

    def _next_batch_classification(self):
        # # with tf.device('/cpu:0'):
        imgs = self.file_name[self.X_key][int(self.current)]
        lbls = self.file_name[self.y_key][self.current]
        if not list(self.img_size) == imgs[0].shape:
            imgs = self._resize_data(imgs)
        if self.augment_dict:
            imgs = self._augment_data(imgs)
        yield imgs, lbls
        for num, item in enumerate(self.iterator, 1):
            imgs = self.file_name[self.X_key][item]
            lbls = self.file_name[self.y_key][item]
            if not list(self.img_size) == imgs[0].shape:
                imgs = self._resize_data(imgs)
            if self.augment_dict:
                imgs = self._augment_data(imgs)
            self.current = item
            if num == self.batch_size:
                break
            yield imgs, lbls
        else:
            self.on_going = False


    def _next_batch_detection(self):
        imgs = self.file_name[self.X_key][int(self.current)]
        lbls = self.file_name[self.y_key][self.current]
        lbls = np.delete(lbls, np.where(lbls.sum(axis=1) == 0), axis=0)
        if not list(self.img_size) == imgs[0].shape:
            imgs = self._resize_data(imgs)
        if self.augment_dict:
            imgs = self._augment_data(imgs)
        yield imgs, lbls
        for num, item in enumerate(self.iterator, 1):
            imgs = self.file_name[self.X_key][item]
            lbls = self.file_name[self.y_key][item]
            lbls = np.delete(lbls, np.where(lbls.sum(axis=1) == 0), axis=0)
            if not list(self.img_size) == imgs[0].shape:
                imgs = self._resize_data(imgs)
            if self.augment_dict:
                imgs = self._augment_data(imgs)
            self.current = item
            if num == self.batch_size:
                break
            yield imgs, lbls
        else:
            self.on_going = False


    def _resize_data(self, imgs, msks=None):
        """

        :param imgs:
        :param msks:
        :return:
        """
        # with tf.device('/cpu:0'):
        if msks is None:
            imgs = cv2.resize(imgs, (self.img_size[1], self.img_size[0]), interpolation=cv2.INTER_LINEAR)
            return imgs
        else:
            imgs = cv2.resize(imgs, (self.img_size[1], self.img_size[0]), interpolation=cv2.INTER_LINEAR)
            msks = cv2.resize(msks, (self.img_size[1], self.img_size[0]), interpolation=cv2.INTER_LINEAR)
            return imgs, msks

    def _augment_data(self, imgs, msks=None):
        """

        :param n:
        :param imgs:
        :param msks:
        :return:
        """
        # with tf.device('/cpu:0'):
        if msks is None:
            if self.augment_dict['flip_hor']:
                if np.random.randint(2) == 1:
                    imgs = np.flip(imgs, axis=0)
            if self.augment_dict['flip_vert']:
                if np.random.randint(2) == 1:
                    imgs = np.flip(imgs, axis=1)
            return imgs
        else:
            if self.augment_dict['flip_hor']:
                if np.random.randint(2) == 1:
                    imgs = np.flip(imgs, axis=0)
                    msks = np.flip(msks, axis=0)
            if self.augment_dict['flip_vert']:
                if np.random.randint(2) == 1:
                    imgs = np.flip(imgs, axis=1)
                    msks = np.flip(msks, axis=1)
            return imgs, msks

    def _recode_masks(self, msks):
        # with tf.device('/cpu:0'):
            # only if segmentation colors are encoded as class labels or binary
        if ((len(msks.shape) == 3) and (np.array_equal(msks[:, :, 0], msks[:, :, 1]))) or (len(msks.shape) != 3):
            new_img = np.zeros([msks.shape[0], msks.shape[1], self.num_lbls])
            # binary segmentation - known bag: binary segmentation only
            # if self.num_lbls == 2:
            #     img = img//255
            #     new_img[:, :, 0] = img
            #     new_img[:, :, 1] = 1 - img
            # else:
            if self.num_lbls >2:
                img = msks[:, :, 0]
                for j in range(0, self.num_lbls):
                    x_ind, y_ind = np.where(img == j)
                    for ind in range(0, len(x_ind)):
                        new_img[x_ind[[ind]], y_ind[ind], j] = 1
            else:
                new_img = msks
            return new_img
        else:  # if segmentation colors are encoded as rgb
            if self.colormap and isinstance(self.colormap, dict) and (len(self.colormap) == self.num_lbls):
                new_img = np.zeros([msks.shape[0], msks.shape[1], self.num_lbls])
                for k, v in self.colormap:
                    x_ind, y_ind = np.where(
                        (msks[:, :, 0] == v[0]) & (msks[:, :, 1] == v[1]) & (msks[:, :, 2] == v[2]))
                    for ind in range(0, len(x_ind)):
                        new_img[x_ind[[ind]], y_ind[ind], k] = 1
                return new_img
            else:
                raise ValueError("Colormap is required in a dictionary format {label: (R, G, B}")
