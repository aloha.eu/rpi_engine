# Requirements

RPI (Refinement for Parsimonious Inference) is based on the NeMO library (NEural Minimizer for tOrch), which has only minimal dependencies on PyTorch 1.1.0, TensorboardX and H5Py. To set up a minimum working environment in Anaconda/Miniconda, run

```
conda create -n aloha python=3
source activate aloha
conda install -y pytorch torchvision -c pytorch
conda install -y scipy
pip install tensorboardX
```

To set up NeMO, update submodules:
```
git submodule init
git submodule update
```

