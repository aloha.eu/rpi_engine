import torch
from torchvision import datasets, transforms
from models.models import Dataset
import h5py
from torch.utils.data import Dataset as TorchDataset
import numpy as np
import os

SHARED_DATA_FOLDER = "/opt/data/"


class H5Dataset(TorchDataset):

    def __init__(self, dataset_id, training, nb_train=False, nb_validation=False):
        super(H5Dataset, self).__init__()

        # Retrieve metadata from mongodb
        self._dataset_id = dataset_id
        self._dataset = Dataset.objects.get(project=str(self._dataset_id))

        self._file_path = os.path.join(SHARED_DATA_FOLDER, self._dataset.path)
        self._data_idx = np.array(self._dataset.slice_split)[0]
        if training is False:
            self._data_idx = np.array(self._dataset.slice_split)[1]
            self._nb_samples = nb_validation
        else:
            self._nb_samples = nb_train

        self._data_idx = sorted(self._data_idx[:self._nb_samples])

        if self._nb_samples is False:
            # todo resolve this problem
            pass

        # FIXME this is only defined for classification
        self._file = h5py.File(self._file_path, 'r')
        self._samples = self._file['X_data'][self._data_idx, ...].transpose(0, 3, 1, 2)
        self._labels = torch.Tensor([np.where(r == 1) for r in self._file['y_data'][self._data_idx]]).flatten().long()
        self._file.close()

    def __getitem__(self, index):
        input = self._samples[index, ...]
        return input.astype('float32'), self._labels[index]

    def __len__(self):
        return self._samples.shape[0]


def load_dataset_from_torch(dataset_name, nb_train, nb_test):
    trsf = transforms.Compose([transforms.Resize(32),
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    tr, val = None, None
    if dataset_name == "mnist":
        tr = datasets.MNIST(root="data/datasets/mnist", train=True, download=True, transform=trsf)
        val = datasets.MNIST(root="data/datasets/mnist", train=False, download=True, transform=trsf)
    elif dataset_name == "cifar10":
        tr = datasets.CIFAR10(root="data/datasets/cifar10", train=True, download=True, transform=trsf)
        val = datasets.CIFAR10(root="data/datasets/cifar10", train=False, download=True, transform=trsf)
    elif dataset_name == "fashion-mnist":
        tr = datasets.FashionMNIST(
            root="data/datasets/fashion-mnist", train=True, download=True, transform=trsf)
        val = datasets.FashionMNIST(
            root="data/datasets/fashion-mnist", train=False, download=True, transform=trsf)
    elif dataset_name == "letters":
        tr = datasets.EMNIST(
            root="data/datasets/emnist", train=True, split='letters', download=True, transform=trsf)
        val = datasets.EMNIST(
            root="data/datasets/emnist", train=False, split='letters', download=True, transform=trsf)

        tr.train_labels -= 1
        val.test_labels -= 1
        tr = torch.utils.data.Subset(tr, np.where(tr.train_labels < 10)[0])
        val = torch.utils.data.Subset(val, np.where(val.test_labels < 10)[0])
    # elif dataset_name == "stl10":
    #     tr = datasets.STL10(root="data/datasets", split='train', download=True, transform=trsf)
    #     val = datasets.STL10(root="data/datasets", split='test', download=True, transform=trsf)
    # elif dataset_name == "svhn":
    #     tr = datasets.SVHN(root="data/datasets", split='train', download=True, transform=trsf)
    #     val = datasets.SVHN(root="data/datasets", split='test', download=True, transform=trsf)
    return torch.utils.data.Subset(tr, range(nb_train)), torch.utils.data.Subset(val, range(nb_test))


class CustomDatasetLoader:

    def __init__(self, dataset_id, batch_size, shuffle=False, nb_train=False, nb_validation=False):
        if dataset_id in ("mnist", "fashion-mnist", "cifar10", "stl10", "svhn", "letters"):
            self._train_dataset, self._validation_dataset = load_dataset_from_torch(dataset_id, nb_train, nb_validation)
        else:
            self._train_dataset      = H5Dataset(dataset_id=dataset_id, training=True, nb_train=nb_train)
            self._validation_dataset = H5Dataset(dataset_id=dataset_id, training=False, nb_validation=nb_validation)

        self._batch_size = batch_size
        self._shuffle = shuffle

    def get_train_validation(self):

        train_loader = torch.utils.data.DataLoader(self._train_dataset,
                                                   batch_size=self._batch_size,
                                                   shuffle=self._shuffle)
        validation_loader = torch.utils.data.DataLoader(self._validation_dataset,
                                                        batch_size=self._batch_size,
                                                        shuffle=self._shuffle)

        return train_loader, validation_loader
